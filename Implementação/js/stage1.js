var stage1State ={

	create: function(){




		//tela de fundo
		game.add.sprite(0,0,'bg');
		this.mascote = game.add.sprite(580,120,'mascote01');
		//flegue para contar quantas palavras foram montadas
		this.conclusao=[false,false,false];


		var enunciado =game.add.text(60,120,'As palavras a serem formadas são:',{font:'18px arial',fill:'#310039'});
		var palavras =game.add.text(60,150,'Casa, Gato, Rato',{font:'24px arial',fill:'#fff'});


		bounds = new Phaser.Rectangle(80, 80, 500, 420);

	    //  Create a graphic so you can see the bounds
	    var graphics = game.add.graphics(bounds.x, bounds.y);
	    graphics.drawRect(0, 0, bounds.width, bounds.height);

	    /*sprite = game.add.sprite(300, 300, 'atari');

	    sprite.inputEnabled = true;
	    sprite.anchor.set(0.5);

	    sprite.input.enableDrag();
	    sprite.input.boundsRect = bounds;*/

		this.cartasMat=[
		['C','A','S','A'],
		['R','A','T','O'],
		['G','A','T','O']
		];


		button = game.add.button(630, 30, 'BTN03', this.sair, this, 1, 0, 2);
		button = game.add.button(550, 30, 'BTN02', this.info, this, 1, 0, 2);
		button = game.add.button(470, 30, 'BTN01', this.novoJogo, this, 1, 0, 2);

this.blocks =game.add.group();
this.blocks.inputEnableChildren = true;

this.coluna1=[];
this.coluna2=[];
this.coluna3=[];
this.coluna4=[];
this.escap=[];



		for(var row in this.cartasMat){
			for(var col in this.cartasMat[row]){
				var tile =this.cartasMat[row][col];

				var x=col*100+100;
				var y=row*30+250;

				switch (tile) {
				  case 'A':
				  /*
var sss=this.blocks.create(646, 120, game.rnd.pick('A'));
                    sss.name='teste';
				    sss.inputEnabled = true;
				    sss.anchor.set(0.5);
				    sss.input.enableDrag(false, true);
				    sss.input.boundsRect = bounds;*/


				    var blockA=this.blocks.create(x, y, game.rnd.pick('A'));
                    blockA.name='A';
                    blockA.key='0';
				    blockA.inputEnabled = true;
				    blockA.anchor.set(0.5);
				    blockA.input.enableDrag(false, true);
				    blockA.input.boundsRect = bounds;
				    blockA.input.enableSnap(100, 30, true, true);
				    blockA.LIMITSprite = 10;



//colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockA);
				    }
				    if(x==200){
				    	this.coluna2.push(blockA);
				    }
				    if(x==300){
				    	this.coluna3.push(blockA);
				    }
				    if(x==400){
				    	this.coluna4.push(blockA);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }


				    //Instruções executadas quando o resultado da expressão for igual á valor1
				    break;
				  case 'C':
				  	var blockC=this.blocks.create(x, y, game.rnd.pick('C'));
				  	blockC.inputEnabled = true;
				  	blockC.key='1';
				    blockC.anchor.set(0.5);
				    blockC.name='C';
				    blockC.input.enableDrag(false, true);
				    blockC.input.boundsRect = bounds;
				    blockC.input.enableSnap(100, 30, true, true);
				    //Instruções executadas quando o resultado da expressão for igual á valor2
				    //colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockC);
				    }
				    if(x==200){
				    	this.coluna2.push(blockC);
				    }
				    if(x==300){
				    	this.coluna3.push(blockC);
				    }
				    if(x==400){
				    	this.coluna4.push(blockC);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }



				    break;
				  case 'S':
				  	var blockS=this.blocks.create(x, y, game.rnd.pick('S'));
				  	blockS.inputEnabled = true;
				  	blockS.key='1';
				    blockS.anchor.set(0.5);
				    blockS.name='S';
				    blockS.input.enableDrag(false, true);
				    blockS.input.boundsRect = bounds;
				    blockS.input.enableSnap(100, 30, true, true);
				    //Instruções executadas quando o resultado da expressão for igual á valorN
//colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockS);
				    }
				    if(x==200){
				    	this.coluna2.push(blockS);
				    }
				    if(x==300){
				    	this.coluna3.push(blockS);
				    }
				    if(x==400){
				    	this.coluna4.push(blockS);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }
				    break;
				  case 'R':
				 	var blockR=this.blocks.create(x, y, game.rnd.pick('R'));
				 	blockR.inputEnabled = true;
				 	blockR.key='1';
				    blockR.anchor.set(0.5);
				    blockR.name='R';
				    blockR.input.enableDrag(false, true);
				    blockR.input.boundsRect = bounds;
				    blockR.input.enableSnap(100, 30, true, true);
//colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockR);
				    }
				    if(x==200){
				    	this.coluna2.push(blockR);
				    }
				    if(x==300){
				    	this.coluna3.push(blockR);
				    }
				    if(x==400){
				    	this.coluna4.push(blockR);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }
				    //Instruções executadas quando o resultado da expressão for igual á valor1
				    break;
				  case 'T':
					  var blockT=this.blocks.create(x, y, game.rnd.pick('T'));
					  blockT.inputEnabled = true;
				    blockT.anchor.set(0.5);
				    blockT.key='1';
				    blockT.name='T';
				    blockT.input.enableDrag(false, true);
				    blockT.input.boundsRect = bounds;
				    blockT.input.enableSnap(100, 30, true, true);
				    //Instruções executadas quando o resultado da expressão for igual á valor2
				    //colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockT);
				    }
				    if(x==200){
				    	this.coluna2.push(blockT);
				    }
				    if(x==300){
				    	this.coluna3.push(blockT);
				    }
				    if(x==400){
				    	this.coluna4.push(blockT);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }
				    break;
				  case 'O':
					  var blockO=this.blocks.create(x, y, game.rnd.pick('O'));
					  blockO.inputEnabled = true;
				    blockO.anchor.set(0.5);
				    blockO.key='0';
				    blockO.name='O';
				    blockO.input.enableDrag(false, true);
				    blockO.input.boundsRect = bounds;
				    blockO.input.enableSnap(100, 30, true, true);

				    //Instruções executadas quando o resultado da expressão for igual á valorN
				    //colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockO);
				    }
				    if(x==200){
				    	this.coluna2.push(blockO);
				    }
				    if(x==300){
				    	this.coluna3.push(blockO);
				    }
				    if(x==400){
				    	this.coluna4.push(blockO);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }
				    break;
				  case 'G':
				  	var blockG=this.blocks.create(x, y, game.rnd.pick('G'));
				  	blockG.inputEnabled = true;
				  	blockG.key='1';
				    blockG.anchor.set(0.5);
				    blockG.name='G';
				    blockG.input.enableDrag(false, true);
				    blockG.input.boundsRect = bounds;
				    blockG.input.enableSnap(100, 30, true, true);
				    //Instruções executadas quando o resultado da expressão for igual á valorN
				   	//colocando nos arreis colunas de palavras que sera verificado no updata para ver se a palavra foi formada
				    if(x==100){
				    	this.coluna1.push(blockG);
				    }
				    if(x==200){
				    	this.coluna2.push(blockG);
				    }
				    if(x==300){
				    	this.coluna3.push(blockG);
				    }
				    if(x==400){
				    	this.colun4.push(blockG);
				    }
				    if(x==500){
				    	this.escap.push(blockA);
				    }break;


				  default:
				    //Instruções executadas quando o valor da expressão é diferente de todos os cases
				    break;
				}





			}

		}
		


	//retorna o nome do objeto da posição 1 do grupo blocks
	
this.blocks.getAt(1).draggable=false;
	},

	update: function(){
		this.desabilitarcarta();
		this.abilitarcarta();
		

//verificacao da coluna 1
		for(var i=this.coluna1.length-1;i>-2;i--){
			if(this.coluna1.length!=0){
			if(this.coluna1[i].position.x==100){
				if(this.coluna1.length==0){
					this.coluna1.push(this.coluna1[i]);
					this.coluna1.pop();
					break;
				}
				else{
					if(this.coluna1[i].key!= this.coluna1[this.coluna1.length-1].key  ){
						this.coluna1.push(this.coluna1[i]);
						this.coluna1.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna1[i].position.x==200){
				if(this.coluna2.length==0){
					this.coluna2.push(this.coluna1[i]);
					this.coluna1.pop();
					break;
				}
				else{
					if(this.coluna1[i].key!= this.coluna2[this.coluna2.length-1].key  ){
						this.coluna2.push(this.coluna1[i]);
						this.coluna1.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna1[i].position.x==300){
				if(this.coluna3.length==0){
					this.coluna3.push(this.coluna1[i]);
					this.coluna1.pop();
					break;
				}
				else{
					if(this.coluna1[i].key!= this.coluna3[this.coluna3.length-1].key  ){
						this.coluna3.push(this.coluna1[i]);
						this.coluna1.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna1[i].position.x==400){
				if(this.coluna4.length==0){
					this.coluna4.push(this.coluna1[i]);
					this.coluna1.pop();
					break;
				}
				else{
					if(this.coluna1[i].key!= this.coluna4[this.coluna4.length-1].key  ){
						this.coluna4.push(this.coluna1[i]);
						this.coluna1.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna1[i].position.x==500){
				if(this.escap.length<2){
					this.escap.push(this.coluna1[i]);
					this.coluna1.pop();
					break;
				}else{
					break;
				}
				
			}
		}
		}
//verificacao da coluna 2
		for(var i=this.coluna2.length-1;i>-2;i--){
			if(this.coluna2.length!=0){
			if(this.coluna2[i].position.x==100){
				if(this.coluna1.length==0){
					this.coluna1.push(this.coluna2[i]);
					this.coluna2.pop();
					break;
				}
				else{
					if(this.coluna2[i].key!= this.coluna1[this.coluna1.length-1].key  ){
						this.coluna1.push(this.coluna2[i]);
						this.coluna2.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna2[i].position.x==200){
				if(this.coluna2.length==0){
					this.coluna2.push(this.coluna2[i]);
					this.coluna2.pop();
					break;
				}
				else{
					if(this.coluna2[i].key!= this.coluna2[this.coluna2.length-1].key  ){
						this.coluna2.push(this.coluna2[i]);
						this.coluna2.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna2[i].position.x==300){
				if(this.coluna3.length==0){
					this.coluna3.push(this.coluna2[i]);
					this.coluna2.pop();
					break;
				}
				else{
					if(this.coluna2[i].key!= this.coluna3[this.coluna3.length-1].key  ){
						this.coluna3.push(this.coluna2[i]);
						this.coluna2.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna2[i].position.x==400){
				if(this.coluna4.length==0){
					this.coluna4.push(this.coluna2[i]);
					this.coluna2.pop();
					break;
				}
				else{
					if(this.coluna2[i].key!= this.coluna4[this.coluna4.length-1].key  ){
						this.coluna4.push(this.coluna2[i]);
						this.coluna2.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.colina2[i].position.x==500){
				if(this.escap.length<2){
					this.escap.push(this.coluna2[i]);
					this.coluna2.pop();
					break;
				}else{
					break;
				}

			}
		}
		}
		//verificacao da coluna 3
		for(var i=this.coluna3.length-1;i>-2;i--){
			if(this.coluna3.length!=0){
			if(this.coluna3[i].position.x==100){
				if(this.coluna1.length==0){
					this.coluna1.push(this.coluna3[i]);
					this.coluna3.pop();
					break;
				}
				else{
					if(this.coluna3[i].key!= this.coluna1[this.coluna1.length-1].key  ){
						this.coluna1.push(this.coluna3[i]);
						this.coluna3.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna3[i].position.x==200){
				if(this.coluna2.length==0){
					this.coluna2.push(this.coluna3[i]);
					this.coluna3.pop();
					break;
				}
				else{
					if(this.coluna3[i].key!= this.coluna2[this.coluna2.length-1].key  ){
						this.coluna2.push(this.coluna3[i]);
						this.coluna3.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna3[i].position.x==300){
				if(this.coluna3.length==0){
					this.coluna3.push(this.coluna3[i]);
					this.coluna3.pop();
					break;
				}
				else{
					if(this.coluna3[i].key!= this.coluna3[this.coluna3.length-1].key  ){
						this.coluna3.push(this.coluna3[i]);
						this.coluna3.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna3[i].position.x==400){
				if(this.coluna4.length==0){
					this.coluna4.push(this.coluna3[i]);
					this.coluna3.pop();
					break;
				}
				else{
					if(this.coluna3[i].key!= this.coluna4[this.coluna4.length-1].key  ){
						this.coluna4.push(this.coluna3[i]);
						this.coluna3.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna3[i].position.x==500){
				if(this.escap.length<2){
					this.escap.push(this.coluna3[i]);
					this.coluna3.pop();
					break;
				}else{
					break;
				}

			}
		}
		}
		//verificacao da coluna 4
		for(var i=this.coluna4.length-1;i>-2;i--){
			if(this.coluna4.length!=0){
			if(this.coluna4[i].position.x==100){
				if(this.coluna1.length==0){
					this.coluna1.push(this.coluna4[i]);
					this.coluna4.pop();
					break;
				}
				else{
					if(this.coluna4[i].key!= this.coluna1[this.coluna1.length-1].key  ){
						this.coluna1.push(this.coluna4[i]);
						this.coluna4.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna4[i].position.x==200){
				if(this.coluna2.length==0){
					this.coluna2.push(this.coluna4[i]);
					this.coluna4.pop();
					break;
				}
				else{
					if(this.coluna4[i].key!= this.coluna2[this.coluna2.length-1].key  ){
						this.coluna2.push(this.coluna4[i]);
						this.coluna4.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna4[i].position.x==300){
				if(this.coluna3.length==0){
					this.coluna3.push(this.coluna4[i]);
					this.coluna4.pop();
					break;
				}
				else{
					if(this.coluna4[i].key!= this.coluna3[this.coluna3.length-1].key  ){
						this.coluna3.push(this.coluna4[i]);
						this.coluna4.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna4[i].position.x==400){
				if(this.coluna4.length==0){
					this.coluna4.push(this.coluna4[i]);
					this.coluna4.pop();
					break;
				}
				else{
					if(this.coluna4[i].key!= this.coluna4[this.coluna4.length-1].key  ){
						this.coluna4.push(this.coluna4[i]);
						this.coluna4.pop();
						break;
					}else{
						break;
					}
				}
			}
			if(this.coluna4[i].position.x==500){
				if(this.escap.length<2){
					this.escap.push(this.coluna4[i]);
					this.coluna4.pop();
					break;
				}else{
					break;
				}
			}
		}
		}


//movimentos de cartas do scap para as demais colunas
		for(var i= 0; i<this.escap.length;i++){
			if(this.escap.length!=0){
//verifica se a carta foi mudada para a coluna 1
			if(this.escap[i].position.x==100){
//verifica se a coluna pra onde a carta foi alterada esta vasia 
				if(this.coluna1.length==0){
					this.coluna1.push(this.escap[i]);
					if(i==0){
						this.escap.shift();
						break;
					}
					if(i!=0){
						this.escap.pop();
						break;
					}	
				}
				else{
//verificando se existe repetição de vogal ou consoante
					if(this.escap[i].key!= this.coluna1[this.coluna1.length-1].key){
						this.coluna1.push(this.escap[i]);
						if(i==0){
							this.escap.shift();
							break;
						}
						if(i!=0){
							this.escap.pop();
							break;
						}	

					}else{
						break;
					}
				}
			}

			if(this.escap[i].position.x==200){
//verifica se a coluna pra onde a carta foi alterada esta vasia 
				if(this.coluna2.length==0){
					this.coluna2.push(this.escap[i]);
					if(i==0){
						this.escap.shift();
						break;
					}
					if(i!=0){
						this.escap.pop();
						break;
					}	
				}
				else{
//verificando se existe repetição de vogal ou consoante
					if(this.escap[i].key!= this.coluna2[this.coluna2.length-1].key){
						this.coluna2.push(this.escap[i]);
						if(i==0){
							this.escap.shift();
							break;
						}
						if(i!=0){
							this.escap.pop();
							break;
						}	

					}else{
						break;
					}
				}
			}

			
			if(this.escap[i].position.x==300){
				//verifica se a coluna pra onde a carta foi alterada esta vasia 
				if(this.coluna3.length==0){
					this.coluna3.push(this.escap[i]);
					if(i==0){
						this.escap.shift();
						break;
					}
					if(i!=0){
						this.escap.pop();
						break;
					}	
				}
				else{
//verificando se existe repetição de vogal ou consoante
					if(this.escap[i].key!= this.coluna3[this.coluna3.length-1].key){
						this.coluna3.push(this.escap[i]);
						if(i==0){
							this.escap.shift();
							break;
						}
						if(i!=0){
							this.escap.pop();
							break;
						}	

					}else{
						break;
					}
				}
			}
			
			if(this.escap[i].position.x==400){
//verifica se a coluna pra onde a carta foi alterada esta vasia 
				if(this.coluna4.length==0){
					this.coluna4.push(this.escap[i]);
					if(i==0){
						this.escap.shift();
						break;
					}
					if(i!=0){
						this.escap.pop();
						break;
					}	
				}
				else{
//verificando se existe repetição de vogal ou consoante
					if(this.escap[i].key!= this.coluna4[this.coluna4.length-1].key){
						this.coluna4.push(this.escap[i]);
						if(i==0){
							this.escap.shift();
							break;
						}
						if(i!=0){
							this.escap.pop();
							break;
						}	

					}else{
						break;
					}
				}
			}

		/*if(this.escap[i].position.x==500){
				//verifica se a coluna pra onde a carta foi alterada esta vasia 
				if(this.escap.length==0){
					this.escap.push(this.escap[i]);
					if(i==0){
						this.escap.shift();
						break;
					}
					if(i!=0){
						this.escap.pop();
						break;
					}	
				}
				else{
//verificando se existe repetição de vogal ou consoante
					if(this.escap[i].key!= this.escap[this.escap.length-1].key){
						this.escap.push(this.escap[i]);
						if(i==0){
							this.escap.shift();
							break;
						}
						if(i!=0){
							this.escap.pop();
							break;
						}	

					}else{
						break;
					}
				}
			}*/
		}}
		
		
//imprimindo palavras formada em cada cluna
		if(this.coluna1.length!=0){
			var palavra1="";
			for(var e=0;e<this.coluna1.length;e++){
				palavra1=""+palavra1+this.coluna1[e].name;
			}
			var palavra2="";
			for(var e=0;e<this.coluna2.length;e++){
				palavra2=""+palavra2+this.coluna2[e].name;
			}
			var palavra3="";
			for(var e=0;e<this.coluna3.length;e++){
				palavra3=""+palavra3+this.coluna3[e].name;
			}
			var palavra4="";
			for(var e=0;e<this.coluna4.length;e++){
				palavra4=""+palavra4+this.coluna4[e].name;
			}
			var palavra5="";
			for(var e=0;e<this.escap.length;e++){
				palavra5=""+palavra5+this.escap[e].name;
			}

			if(palavra1=="GATO" || palavra1=="CASA" || palavra1=="RATO"){
				this.conclusao[1]=true;
				this.coluna1[0].tint = 0x296040;
				this.coluna1[0].inputEnabled = false;
				this.coluna1[1].tint = 0x296040;
				this.coluna1[1].inputEnabled = false;
				this.coluna1[2].tint = 0x296040;
				this.coluna1[2].inputEnabled = false;
				this.coluna1[3].tint = 0x296040;
				this.coluna1[3].inputEnabled = false;
			}
			if(palavra2=="GATO" || palavra2=="CASA" || palavra2=="RATO"){
				this.conclusao[2]=true;
				this.coluna2[0].tint = 0x296040;
				this.coluna2[0].inputEnabled = false;
				this.coluna2[1].tint = 0x296040;
				this.coluna2[1].inputEnabled = false;
				this.coluna2[2].tint = 0x296040;
				this.coluna2[2].inputEnabled = false;
				this.coluna2[3].tint = 0x296040;
				this.coluna2[3].inputEnabled = false;
			}
			if(palavra3=="GATO" || palavra3=="CASA" || palavra3=="RATO"){
				this.conclusao[3]=true;
				this.coluna3[0].tint = 0x296040;
				this.coluna3[0].inputEnabled = false;
				this.coluna3[1].tint = 0x296040;
				this.coluna3[1].inputEnabled = false;
				this.coluna3[2].tint = 0x296040;
				this.coluna3[2].inputEnabled = false;
				this.coluna3[3].tint = 0x296040;
				this.coluna3[3].inputEnabled = false;
			}
			if(palavra4=="GATO" || palavra4=="CASA" || palavra4=="RATO"){
				this.conclusao[4]=true;
				this.coluna4[0].tint = 0x296040;
				this.coluna4[0].inputEnabled = false;
				this.coluna4[1].tint = 0x296040;
				this.coluna4[1].inputEnabled = false;
				this.coluna4[2].tint = 0x296040;
				this.coluna4[2].inputEnabled = false;
				this.coluna4[3].tint = 0x296040;
				this.coluna4[3].inputEnabled = false;
			}


		}
		this.abilitarcarta();


},

render: function(){

var acrecimoy=0;
	for (var a=0; a<this.coluna1.length;a++ ){
		var acrecimoy=250+30*a;
		this.coluna1[a].position.y=acrecimoy;
		this.coluna1[a].position.x=100;
	}

	for (var a=0; a<this.coluna2.length;a++ ){
		var acrecimoy=250+30*a;
		this.coluna2[a].position.y=acrecimoy;
		this.coluna2[a].position.x=200;
	}
	for (var a=0; a<this.coluna3.length;a++ ){
		var acrecimoy=250+30*a;
		this.coluna3[a].position.y=acrecimoy;
		this.coluna3[a].position.x=300;
	}
	for (var a=0; a<this.coluna4.length;a++ ){
		var acrecimoy=250+30*a;
		this.coluna4[a].position.y=acrecimoy;
		this.coluna4[a].position.x=400;

	}
	for (var a=0; a<this.escap.length;a++ ){
		var acrecimoy=220+140*a;
		this.escap[a].position.y=acrecimoy;
		this.escap[a].position.x=500;
	}
//mudar mascote
var cont=0;
for(var i=0;i<this.conclusao.length;i++){
	if(this.conclusao[i]==true){
		cont++;
	}
	if(cont==1){
		this.mascote.loadTexture('mascote04');
		}
	if(cont==2){
		this.mascote.loadTexture('mascote05');
		}
	if(cont==3){
		this.mascote.loadTexture('mascote06');
	}

}
	

	//game.debug.pointer(game.input.mousePointer);

	//retorna o onjeto de um grupo por seu index   .getAt(index) ou getChildAt
	//retorna a dimensção do objeto getBounds ()
	//retorna o filho do topo getTop ()
	

    //game.debug.text('Living baddies: ' + (this.coluna1[this.coluna1.length-1].downPoint ()) , 340, 420);
    //game.debug.inputInfo(32, 32);

},

	
	novoJogo: function  () {
		game.state.start('stage1') 
	},

	sair: function  (sprite) {
		game.state.start('menu') 
	},
	info: function(){
		game.state.start('stage2') 
	},
	abilitarcarta: function  () {
		if(this.coluna1.length!=0){
			
			this.coluna1[this.coluna1.length-1].input.draggable = true;
			this.coluna1[this.coluna1.length-1].input.bringToTop =true;

		}

		if(this.coluna2.length!=0){
			
			this.coluna2[this.coluna2.length-1].input.draggable = true;
			this.coluna2[this.coluna2.length-1].input.bringToTop =true;
		}

		if(this.coluna3.length!=0){
			
			this.coluna3[this.coluna3.length-1].input.draggable = true;
			this.coluna3[this.coluna3.length-1].input.bringToTop =true;
		}

		if(this.coluna4.length!=0){
			
			this.coluna4[this.coluna4.length-1].input.draggable = true;
			this.coluna4[this.coluna4.length-1].input.bringToTop =true;
		}
	},

	desabilitarcarta: function  () {
		for(var i=0; i<this.coluna1.length-1;i++){
			this.coluna1[i].input.draggable = false;
			this.coluna1[i].input.bringToTop =false;
		}
		for(var i=0; i<this.coluna2.length-1;i++){
			this.coluna2[i].input.draggable = false;
			this.coluna2[i].input.bringToTop =false;
		}
		for(var i=0; i<this.coluna3.length-1;i++){
			this.coluna3[i].input.draggable = false;
			this.coluna3[i].input.bringToTop =false;

		}
		for(var i=0; i<this.coluna4.length-1;i++){
			this.coluna4[i].input.draggable = false;
			this.coluna4[i].input.bringToTop =false;
		}

		
	},


}

