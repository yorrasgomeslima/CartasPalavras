var loadState = {
	preload: function(){
		var txtLoading = game.add.text(game.world.centerX,150,'LOADING...',{font:'15px emulogic',fill:'#fff'});
			txtLoading.anchor.set(.5);
	
		var progressBar = game.add.sprite(game.world.centerX,250,'progressBar');
			progressBar.anchor.set(.5);
			
		game.load.setPreloadSprite(progressBar);
		
		game.load.image('bg','img/bg.jpg');
		game.load.image('bg2','img/bg2.jpg');
		game.load.image('bg3','img/bg3.jpg');
		game.load.image('logo','img/logo.png');
		game.load.image('button','img/button.png');
		game.load.image('block','img/block.png');
		game.load.image('end','img/end.png');
		game.load.image('part','img/part.png');
		game.load.image('mascote01','img/mascote01.png');
		game.load.image('mascote02','img/mascote02.png');
		game.load.image('mascote03','img/mascote03.png');
		game.load.image('mascote04','img/mascote04.png');
		game.load.image('mascote05','img/mascote05.png');
		game.load.image('mascote06','img/mascote06.png');

		game.load.image('BTN01','img/BTN01.png');
		game.load.image('BTN02','img/BTN02.png');
		game.load.image('BTN03','img/BTN03.png');



		

		game.load.image('A','img/A.png');
		game.load.image('G','img/G.png');
		game.load.image('C','img/C.png');
		game.load.image('S','img/S.png');
		game.load.image('R','img/R.png');
		game.load.image('T','img/T.png');
		game.load.image('O','img/O.png');

		
		game.load.spritesheet('coin','img/coin.png',32,32);
		game.load.spritesheet('enemy','img/enemy.png',24,40);
		game.load.spritesheet('player','img/player.png',24,32);
		
		game.load.audio('getitem','sfx/getitem.ogg');
		game.load.audio('loseitem','sfx/loseitem.ogg');
		game.load.audio('music','sfx/music.ogg');

		game.physics.startSystem(Phaser.Physics.ARCADE);
		
	},

	create: function(){
		game.state.start('menu');
	}
};
