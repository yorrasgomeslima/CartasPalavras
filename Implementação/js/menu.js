var menuState={
	create: function(){
		this.music = game.add.audio('music');
		//musica em loop
		this.music.loop=true;
		//volume da musica
		this.music.volume=.5;
		//começar tocando
		this.music.play();

		game.add.sprite(0,0,'bg2');

		var button;
var background;
game.load.spritesheet('button', 'button', 90,90);
    	game.load.image('background','assets/misc/starfield.jpg');


		var txtPressStart = game.add.sprite(game.world.centerX,550,'logo',{font:'20px arial',fill:'#fff'});
			txtPressStart.anchor.set(.5);

			//animação efeito no texto  to(possoção final, velocidade)
		game.add.tween(txtPressStart).to({y:150},2000).start();
			// esperar tempo (tempo, função, contexto)
		game.time.events.add(1000,function(){
			// criando um botão como entrada
			var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
				enterkey.onDown.addOnce(this.startGame,this);

		},this);



    // The numbers given in parameters are the indexes of the frames, in this order: over, out, down
     button= game.add.button(game.world.centerX, 800, 'button', this.actionOnClick, this, 1, 0, 2);
    game.add.tween(button).to({y:300},2000).start();
    //  setting the anchor to the center
    button.anchor.setTo(0.5, 0.5);


	},

//chamr o stage1
	startGame: function(){
		this.music.stop();
		game.state.start('stage1')

	},

	actionOnClick: function  () {
		this.music.stop();
		game.state.start('stage1') 
}


};